//
//  AkhileshViewController.h
//  PlayingWithBitBucket
//
//  Created by Akhilesh Mishra on 17/02/15.
//  Copyright (c) 2015 Akhilesh Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AkhileshViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) IBOutlet UILabel *label;

- (IBAction)ClickToSee:(id)sender;
@end
