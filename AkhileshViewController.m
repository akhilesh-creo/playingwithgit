//
//  AkhileshViewController.m
//  PlayingWithBitBucket
//
//  Created by Akhilesh Mishra on 17/02/15.
//  Copyright (c) 2015 Akhilesh Mishra. All rights reserved.
//

#import "AkhileshViewController.h"

@interface AkhileshViewController ()

@end

@implementation AkhileshViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)ClickToSee:(id)sender
{
    NSString *name=self.textField.text;
    self.label.text=[NSString stringWithFormat:@"Welcome %@",name];
}
@end
