//
//  AppDelegate.h
//  PlayingWithBitBucket
//
//  Created by Akhilesh Mishra on 17/02/15.
//  Copyright (c) 2015 Akhilesh Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

