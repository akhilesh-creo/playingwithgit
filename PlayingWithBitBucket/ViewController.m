//
//  ViewController.m
//  PlayingWithBitBucket
//
//  Created by Akhilesh Mishra on 17/02/15.
//  Copyright (c) 2015 Akhilesh Mishra. All rights reserved.
//

#import "ViewController.h"
#import "AkhileshViewController.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Akhilesh"]) {
        AkhileshViewController *destViewController=[[AkhileshViewController alloc] init];
        destViewController.title=@"Akhilesh";
    }
}
@end
